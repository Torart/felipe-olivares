package Main;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author Brayan Felipe Olivares Montañez - 1024591691
 * @author Jose Deniel Escobar Murcia - 1075313844.
 * @author Eduardo Santiago Vanegas Galvis - 
 * @version 10.02.2018.
 */
public class Client extends Thread {

    @Override
    public void run() {
        startClient();
    }

    public static void startClient() {

        try {

            final int puertoLlegadaServidor = 49751;//Puerto por el que llegan los paquetes al servidor
            final int puertoCliente = 49752;//Puerto por el que llagan los qaquetes al cliente (puerto de llgada).
            DatagramSocket socket = new DatagramSocket(puertoCliente);//Se configura el socket para que escuche en el puerto de llegada.
            socket.setSoTimeout(1000);//Se establece el limite de tiempo para esperar la llegada de un paquete 1000ms = 1s.
            InetAddress ipServidor = InetAddress.getByName("192.168.0.7");//direccion ip del servidor.
            byte[] echoingBytes = new byte[256];//Se establece que el mensaje en bytes que llegara tendra su maximo tamaño (256 bytes).
            DatagramPacket echoingPackage = new DatagramPacket(echoingBytes, 256);//Se establece el mensaje y tamaño que contndran los paquetes que llagaran de vuelta al cliente.

            System.out.println("");
            System.out.println("Resultados:");

            for (int i = 0; i < 10; i++) {//Ronda de 10 pings.

                String mensaje = "ping " + i;//Mensaje enviado al servidor cabe resaltar que esta en minusculas.
                byte[] mensajeBytes = mensaje.getBytes();//Se pasa el mansaje a un arreglo de bytes para que corresponda con el tipo de datos que recivira el servidor.
                DatagramPacket paquete = new DatagramPacket(mensajeBytes, mensaje.length(), ipServidor, puertoLlegadaServidor);//Se establece el mensaje y el destino del paquete.
                long hEnvio = System.currentTimeMillis();//Hora de salida del ping.
                socket.send(paquete);//Se envia el paquete.

                try {

                    socket.receive(echoingPackage);//Se ordena al socket recivir los paquetes establecidos anteriormente.
                    String echoingText = new String(echoingBytes).trim();//Se pasa el mensaje del paquete a un String.
                    long hRespuesta = System.currentTimeMillis();//Se calcula la hora de llagada.
                    long rtt = hRespuesta - hEnvio;//Se calcula el tiempo de ida y vuelta del mensaje en ms.
                    System.out.println(echoingText + " " + rtt + "ms.");//Se imprime el mensaje de llagada con su tiempo de viaje.

                } catch (java.net.SocketTimeoutException e) {//al romperse el limite de espera se lansa al excepcion presente en este cach por lo que se imprime el mensaje enviado y el tiwmpo de espera, 1s.

                    long hRespuesta = System.currentTimeMillis();//Se calcula la hora de llagada.
                    long tl = hRespuesta - hEnvio;//Se calcula el lapso de tiempo transcurrido desde el envio en ms.
                    System.out.println("Solicitud Expirada: " + mensaje + " " + tl + "ms");

                }

            }

            System.out.println("");
            System.exit(0);//Se finaliza el programa como exitoso.

        } catch (IOException e) {

        }

    }

}
