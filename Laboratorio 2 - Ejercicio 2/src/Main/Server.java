package Main;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author Brayan Felipe Olivares Montañez - 1024591691
 * @author Jose Deniel Escobar Murcia - 1075313844.
 * @author Eduardo Santiago Vanegas Galvis - 
 * @version 10.02.2018.
 */
public class Server extends Thread {

    @Override
    public void run() {
        startServer();//Se inicia el servidor como un subproceso mediante un hilo.
    }

    public static void startServer() {

        try {

            final int puertoLlegadaServidor = 58791;//Puerto por el que llegan los paquetes al servidor
            byte[] mensajeBytes = new byte[256];//Se establece que el mensaje en bytes que llegara tendra su maximo tamaño (256 bytes).
            DatagramSocket socket = new DatagramSocket(puertoLlegadaServidor);//Se establece el puerto por el que escuchara el socket del servidor.
            DatagramPacket paquete = new DatagramPacket(mensajeBytes, 256);//Se establece el mensaje y tamaño que contndran los paquetes que llagaran al servidor.

            while (true) {//Se inicia el bucle infinito de escucha de paquetes.

                socket.receive(paquete);
                int puertoCliente = paquete.getPort();//Se obtiene el puerto por el que envio el cliente.
                InetAddress ipCliente = paquete.getAddress();// Se obtiene la ip de la que envio el cliente.
                String mensajeLlegada = new String(mensajeBytes).trim();//Con el metodo trim se desncapsula y pasa el paquete binario a String.
                String mensajeRespuesta = mensajeLlegada.toUpperCase();//Se´pone el mensaje en letras mayusculas.
                System.out.println(" Mensaje Respuesta: "+mensajeRespuesta +"    ||   Mensaje Llegada: "+ mensajeLlegada);
                
                DatagramPacket paqueteRespuesta = new DatagramPacket(((mensajeRespuesta).getBytes()), mensajeLlegada.length(), ipCliente, puertoCliente);//Se establecen los datos y el destino del paquete.
                socket.send(paqueteRespuesta);//Se envia el paquete establecido en la linea anterior.

            }

        } catch (IOException e) {

        }

    }

}
