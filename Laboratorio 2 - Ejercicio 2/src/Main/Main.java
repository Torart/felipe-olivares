package Main;

import java.util.Scanner;

/**
 * Clase encargada de iniciar las actividades del servidor y del cliente en
 * hilos aparte.
 *
 * @author Brayan Felipe Olivares Montañez - 1024591691
 * @author Jose Deniel Escobar Murcia - 1075313844.
 * @author Eduardo Santiago Vanegas Galvis - 
 * @version 10.02.2018.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {

        Scanner sc = new Scanner(System.in);

        System.out.print("Para iniciar el ping presione 1: ");
        int iniciar = sc.nextInt();

        if (iniciar == 1) {

            new Server().start();//Se inicia el servidor en un hilo.
            Thread.sleep(4);//Se le da al servidor una ventaja de 4ms.
            //new Client().start();//Se inicial el cliente en un hilo aparte.

        } else {

            System.out.println("");
            System.exit(0);//Se finaliza el programa como exitoso.

        }

    }

}
